using System;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using NSubstitute;
using NUnit.Framework;
using TicketServiceLibrary.DynamoDB;
using TicketServiceLibrary.Models;

namespace TicketServiceLibraryTests
{
    /// <summary>
    /// DISCLAIMER: Was unable to unit test this class properly due to an issue in the IDynamoDBContext class not being fully mock-able.
    /// I left the code here anyway so you could see the attempt.
    /// </summary>
    public class Tests
    {
        private DynamoDbRepository _dynamoDbRepository;
        private IDynamoDBContext _dynamoDbContext;
        private IAmazonDynamoDB _amazonDynamoDb;
        
        private void SetupDynamoDbRepository()
        {
            _dynamoDbContext = Substitute.For<IDynamoDBContext>();
            _amazonDynamoDb = Substitute.For<IAmazonDynamoDB>();
            _dynamoDbRepository = new DynamoDbRepository(_dynamoDbContext, _amazonDynamoDb);
        }

        [Test]
        public async Task DynamoDbRepository_Create_CanBeExecuted()
        {
            SetupDynamoDbRepository();
            string ticketId = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticketsDatabaseModel = new TicketsDatabaseModel(ticketId, DateTime.UtcNow, true);
            TicketsDatabaseModel ticketsDatabaseModelAfterCreate = await _dynamoDbRepository.Create(ticketsDatabaseModel);
            Assert.AreEqual(ticketsDatabaseModel, ticketsDatabaseModelAfterCreate);
        }
        
        /// <summary>
        /// I was unable to mock some data structures needed to properly unit test the IDynamoDBContext.
        /// I stopped trying as it seems many people had a similar issue. I left the attempt here to show the issue.
        /// https://github.com/aws/aws-sdk-net/issues/1310
        /// </summary>
        [Test]
        public async Task DynamoDbRepository_Read_CanBeExecuted()
        {
            throw new NotImplementedException();
            
            Table table = (Table)Activator.CreateInstance(typeof(Table), true, new object[]
            {
                _amazonDynamoDb,
                new TableConfig("TicketsTable")
            });
            
            SetupDynamoDbRepository();

            _dynamoDbContext.GetTargetTable<TicketsDatabaseModel>().Returns(table);
            _dynamoDbContext.GetTargetTable<TicketsDatabaseModel>().GetItemAsync(Arg.Any<string>()).
                Returns(Arg.Any<Document>());

            string ticketGuid = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticketsDatabaseModelToReturn = new TicketsDatabaseModel(ticketGuid, DateTime.UtcNow, true);
            _dynamoDbContext.FromDocument<TicketsDatabaseModel>(Arg.Any<Document>()).Returns(ticketsDatabaseModelToReturn);
            
            TicketsDatabaseModel ticketsDatabaseModelAfterRead = await _dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid);
            
            Assert.AreEqual(ticketsDatabaseModelToReturn, ticketsDatabaseModelAfterRead);
        }
    }
}