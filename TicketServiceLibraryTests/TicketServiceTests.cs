﻿using System;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using TicketServiceLibrary.Interfaces;
using TicketServiceLibrary.Models;
using TicketServiceLibrary.Services;

namespace TicketServiceLibraryTests
{
    public class TicketServiceTests
    {
        [Test]
        public async Task TicketService_Tickets_CanBeCreated()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            string ticketGuid = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticketsDatabaseModel = new (ticketGuid, DateTime.UtcNow, true);
            dynamoDbRepository.Create(Arg.Any<TicketsDatabaseModel>())
                .Returns(ticketsDatabaseModel);

            TicketsDatabaseModel ticket = await ticketService.CreateTicket();
            Assert.AreEqual(ticketsDatabaseModel, ticket);
        }
        
        [Test]
        public async Task TicketService_GetTicket_ReturnsATicket()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            string ticketGuid = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticketsDatabaseModel = new (ticketGuid, DateTime.UtcNow, true);
            dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid)
                .Returns(ticketsDatabaseModel);
            
            TicketsDatabaseModel ticket = await ticketService.GetTicket(ticketGuid);
            Assert.AreEqual(ticketsDatabaseModel, ticket);
        }
        
        [Test]
        public async Task TicketService_GetTotalTicketCount_ReturnsAnInteger()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            int scannedCount = 1000;
            dynamoDbRepository.ScanTableCount<TicketsDatabaseModel>()
                .Returns(scannedCount);
            
            int ticketCount = await ticketService.GetTotalTicketCount();
            Assert.AreEqual(scannedCount, ticketCount);
        }
        
        [Test]
        public async Task TicketService_Invalidate_ReturnsAnInvalidTicket()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            
            string ticketGuid = Guid.NewGuid().ToString();

            TicketsDatabaseModel ticketsDatabaseModel = new (ticketGuid, DateTime.UtcNow, true);
            dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid)
                .Returns(ticketsDatabaseModel);
            
            Assert.True(ticketsDatabaseModel.Valid);
            
            TicketsDatabaseModel updatedModel = new (ticketGuid, DateTime.UtcNow, false);
            dynamoDbRepository.Update(ticketGuid, Arg.Any<TicketsDatabaseModel>())
                .Returns(updatedModel);
            
            TicketsDatabaseModel ticketAfterBeingInvalidated = await ticketService.InvalidateTicket(ticketGuid);
            
            Assert.AreEqual(ticketGuid, ticketAfterBeingInvalidated.TicketGuid);
            Assert.False(ticketAfterBeingInvalidated.Valid);
        }
        
        [Test]
        public void TicketService_DeleteTicket_CanExecute()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            string ticketGuid = Guid.NewGuid().ToString();
            Assert.DoesNotThrowAsync(async ()=> await ticketService.DeleteTicket(ticketGuid));
        }
        
        [Test]
        public void TicketService_GetTicketGivenNullOrEmptyString_ThrowsArgumentNullException()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            string ticketGuid = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticketsDatabaseModel = new (ticketGuid, DateTime.UtcNow, true);
            dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid)
                .Returns(ticketsDatabaseModel);
            
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.GetTicket(""));
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.GetTicket(null));
        }
        
        [Test]
        public void TicketService_InvalidateGivenNullOrEmptyString_ThrowsArgumentNullException()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            
            string ticketGuid = Guid.NewGuid().ToString();

            TicketsDatabaseModel ticketsDatabaseModel = new (ticketGuid, DateTime.UtcNow, true);
            dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid)
                .Returns(ticketsDatabaseModel);
            
            Assert.True(ticketsDatabaseModel.Valid);
            
            TicketsDatabaseModel updatedModel = new (ticketGuid, DateTime.UtcNow, false);
            dynamoDbRepository.Update(ticketGuid, Arg.Any<TicketsDatabaseModel>())
                .Returns(updatedModel);
            
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.InvalidateTicket(""));
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.InvalidateTicket(null));
        }
        
        [Test]
        public void TicketService_DeleteTicketGivenNullOrEmptyString_ThrowsArgumentNullException()
        {
            IDynamoDbRepository dynamoDbRepository = Substitute.For<IDynamoDbRepository>();
            ITicketService ticketService = new TicketService(dynamoDbRepository);
            
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.DeleteTicket(""));
            Assert.ThrowsAsync(typeof(ArgumentNullException), async () => await ticketService.DeleteTicket(null));
        }
    }
}