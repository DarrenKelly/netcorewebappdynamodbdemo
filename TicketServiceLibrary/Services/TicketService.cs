﻿using System;
using System.Threading.Tasks;
using TicketServiceLibrary.Interfaces;
using TicketServiceLibrary.Models;

namespace TicketServiceLibrary.Services
{
    public class TicketService : ITicketService
    {
        private readonly IDynamoDbRepository _dynamoDbRepository;

        public TicketService(IDynamoDbRepository dynamoDbRepository)
        { 
            _dynamoDbRepository = dynamoDbRepository;
        }

        public async Task<TicketsDatabaseModel> CreateTicket()
        {
            string ticketId = Guid.NewGuid().ToString();
            TicketsDatabaseModel ticket = new TicketsDatabaseModel(ticketId, DateTime.UtcNow, true);
            TicketsDatabaseModel ticketsDatabaseModel = await _dynamoDbRepository.Create(ticket);
            return ticketsDatabaseModel;
        }

        public async Task<TicketsDatabaseModel> InvalidateTicket(string ticketGuid)
        {
            if (string.IsNullOrEmpty(ticketGuid))
            {
                throw new ArgumentNullException();
            }
            
            TicketsDatabaseModel existingTicketModel = await _dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid);
            TicketsDatabaseModel invalidTicket = new TicketsDatabaseModel(existingTicketModel.TicketGuid, existingTicketModel.Date, false);
            TicketsDatabaseModel updatedTicket = await _dynamoDbRepository.Update(ticketGuid, invalidTicket);
            return updatedTicket;
        }
        
        public async Task<TicketsDatabaseModel> GetTicket(string ticketGuid)
        {
            if (string.IsNullOrEmpty(ticketGuid))
            {
                throw new ArgumentNullException();
            }
            
            TicketsDatabaseModel ticket =  await _dynamoDbRepository.Read<TicketsDatabaseModel>(ticketGuid);
            return ticket;
        }

        public async Task<int> GetTotalTicketCount()
        {
            // In a real application we would have one or multiple counters updated as we create tickets.  
            // Could be done using Amazon SQS (updating the counter after creation) or using documents on a different table.
            // Depending on concurrency on the sum we could even partition it and retrieve the total by summing them.

            int scannedTableCount = await _dynamoDbRepository.ScanTableCount<TicketsDatabaseModel>();
            return scannedTableCount;
        }
        
        public async Task DeleteTicket(string ticketGuid)
        {
            if (string.IsNullOrEmpty(ticketGuid))
            {
                throw new ArgumentNullException();
            }
            
            await _dynamoDbRepository.Delete<TicketsDatabaseModel>(ticketGuid);
        }
    }
}