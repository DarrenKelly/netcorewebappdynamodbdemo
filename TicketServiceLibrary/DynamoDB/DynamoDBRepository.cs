﻿using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using TicketServiceLibrary.Interfaces;

namespace TicketServiceLibrary.DynamoDB
{
    public class DynamoDbRepository : IDynamoDbRepository
    {
        private readonly IDynamoDBContext _dynamoDbContext;
        private readonly IAmazonDynamoDB _client;

        public DynamoDbRepository(IDynamoDBContext dynamoDbContext, IAmazonDynamoDB client)
        {
            _dynamoDbContext = dynamoDbContext;
            _client = client;
        }
        
        public async Task<T> Create<T>(T model)
        {
            await _dynamoDbContext.SaveAsync(model);
            return model;
        }
        
        public async Task<T> Read<T>(string hashKey)
        {
            Document document = await _dynamoDbContext.GetTargetTable<T>().GetItemAsync(hashKey);

            if (document == null)
            {
                throw new ResourceNotFoundException(
                    $"Tried to update the resource type {typeof(T)} but the key was not found. key: {hashKey}");
            }
            
            return _dynamoDbContext.FromDocument<T>(document);
        }

        public async Task<T> Update<T>(string hashKey, T updatedModel)
        {
            T modelToUpdate = await Read<T>(hashKey);

            if (modelToUpdate == null)
            {
                throw new ResourceNotFoundException(
                    $"Tried to update the resource type {typeof(T)} but the key was not found. key: {hashKey}");
            }

            modelToUpdate = updatedModel;
            
            await _dynamoDbContext.SaveAsync(modelToUpdate);
            return modelToUpdate;
        }
        
        public async Task Delete<T>(string hashKey)
        {
            T specificItem = await Read<T>(hashKey);
            
            if (specificItem == null)
            {
                throw new ResourceNotFoundException(
                    $"Tried to update the resource type {typeof(T)} but the key was not found. key: {hashKey}");
            }
            
            await _dynamoDbContext.DeleteAsync(specificItem);
        }
        
        public async Task<int> ScanTableCount<T>()
        {
            ScanRequest request = new ScanRequest()
            {
                TableName = _dynamoDbContext.GetTargetTable<T>().TableName,
            };


            ScanResponse scanResponse = await _client.ScanAsync(request);
            return scanResponse.ScannedCount;
        }
    }
}