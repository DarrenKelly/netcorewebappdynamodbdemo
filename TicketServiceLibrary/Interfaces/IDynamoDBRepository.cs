﻿using System.Threading.Tasks;

namespace TicketServiceLibrary.Interfaces
{
    public interface IDynamoDbRepository
    {
        Task<T> Create<T>(T model);
        Task<T> Read<T>(string hashKey);
        Task<T> Update<T>(string hashKey, T updatedModel);
        Task Delete<T>(string hashKey);
        Task<int> ScanTableCount<T>();
    }
}