﻿using System.Threading.Tasks;
using TicketServiceLibrary.Models;

namespace TicketServiceLibrary.Interfaces
{
    public interface ITicketService
    {
        Task<TicketsDatabaseModel> CreateTicket();
        Task<TicketsDatabaseModel> GetTicket(string ticketGuid);
        Task<int> GetTotalTicketCount();
        Task<TicketsDatabaseModel> InvalidateTicket(string ticketGuid);
        Task DeleteTicket(string ticketGuid);
    }
}