using System;
using Amazon.DynamoDBv2.DataModel;

namespace TicketServiceLibrary.Models
{
    [DynamoDBTable("TicketsTable")]
    public class TicketsDatabaseModel
    {
        [DynamoDBHashKey]
        public string TicketGuid { get; set; }

        [DynamoDBProperty]
        public DateTime Date { get; set; }
        
        [DynamoDBProperty]
        public bool Valid { get; set; }

        public TicketsDatabaseModel(){}
        public TicketsDatabaseModel(string ticketGuid, DateTime date, bool valid)
        {
            TicketGuid = ticketGuid;
            Date = date;
            Valid = valid;
        }
    }
}
