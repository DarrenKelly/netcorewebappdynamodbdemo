# Welcome

Welcome to my web app demo!
Here you can find any info and comments about my journey.

The test was a very enjoyable one, Thank you for the oppurtunity.

There are comments below about some of the decisions I made.

## Prerequisites

- Visual Studio or Rider IDE to run the application
- .net5.0 framework

## How To Use

You can run both web apps locally through Visual Studio or Rider.

To make api calls and test I use swagger. The private web app should automatically open on the swagger page for you on a localhost page after you run it. 
The public one has an option you can click to open swagger.

You can use all web api's via swagger.

I recommend running both web apps at the same time so you can copy paste guid's of tickets between both to try out api's.

## Solution Structure

DarrensPrivateWebApp: A private web app project where in a real life application you would limit those api usages to the local network.

DarrensWebApp: A public web app project which can be used by anyone with access. 

TicketsServiceLibrary: A library project which is used by both web applications and the unit test project. 
The library handles the business logic for the ticketService api's and contains the database creating, reading updating and deleting.

TicketsServiceLibraryTests: A unit test project which contains any unit tests necessary for the TicketsServiceLibrary. 
The DynamoDBRepositoryTests tests are not functioning due to running into mocking issues. I left it in there to show the attempt.

## Program

Both web apps contain an identical program class which is setting up the program and deciding which script is used for startup.

## Startup

In both web apps this class handles any setup logic needed. It sets up any necessary classes needed for dependency injection throughout the application.

## TicketsController

Both web apps have a ticketsController class where the web api's are contained.

## DarrensWebApp.HomeController

The main home page controller. Used for navigation.

## TicketsServiceLibrary.DynamoDbRepository

Handles all of the communication with the dynamodb database.

## TicketsServiceLibrary.TicketService

The service which is used by both web applications to handle business logic and communication with the DynamoDbRepository class to write data to the database.

## Outro

I hope you enjoy reading my code, I am looking forward to any feedback you may have! 