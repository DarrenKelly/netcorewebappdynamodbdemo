﻿using System.Threading.Tasks;
using DarrensWebApp.Models;
using Microsoft.AspNetCore.Mvc;
using TicketServiceLibrary.Interfaces;
using TicketServiceLibrary.Models;

namespace DarrensWebApp.Controllers
{
    [Route("tickets")]
    public class TicketsController : Controller
    {
        private readonly ITicketService _ticketService;
        
        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateTicket()
        {
            var ticketsDatabaseModel = await _ticketService.CreateTicket();
            var ticket = new TicketsModel(ticketsDatabaseModel);
            return Ok(ticket);
        }
        
        [Route("{ticketId}/valid")]
        [HttpGet]
        public async Task<bool> IsValidTicket(string ticketId)
        {
             TicketsDatabaseModel ticket = await _ticketService.GetTicket(ticketId);
             return ticket.Valid;
        }
    }
}
