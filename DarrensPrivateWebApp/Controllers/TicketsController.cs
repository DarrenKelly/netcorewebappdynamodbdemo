﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TicketServiceLibrary.Interfaces;
using TicketServiceLibrary.Models;

namespace DarrensPrivateWebApp.Controllers
{
    [ApiController]
    [Route("tickets-private")]
    public class TicketsController : ControllerBase
    {
        private readonly ITicketService _ticketService;
        
        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [Route("/count")]
        [HttpGet]
        public async Task<int> GetTicketCount()
        {
            int totalTicketCount = await _ticketService.GetTotalTicketCount();
            return totalTicketCount;
        }
        
        [Route("{ticketId}/invalidate")]
        [HttpPost]
        public async Task<TicketsDatabaseModel> InvalidateTicket(string ticketId)
        {
            TicketsDatabaseModel ticket = await _ticketService.InvalidateTicket(ticketId);
            return ticket;
        }
        
        [Route("{ticketId}")]
        [HttpDelete]
        public async Task DeleteTicket(string ticketId)
        {
            await _ticketService.DeleteTicket(ticketId);
        }
    }
}
